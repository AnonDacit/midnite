# midnite
![](./assets/images/logo.svg)

This is a framework to gain insight into [pytorch](https://pytorch.org/) deep neural networks for visual recognition tasks.

A brief introduction is in the project's [README](https://gitlab.com/luminovo/midnite/blob/master/README.md).
 
Here, you will find the comprehensive documentation, including apidoc and jupyter notebooks.

## Contents
```eval_rst
.. toctree::
   :maxdepth: 3
   
   getting_started
.. toctree::
   :maxdepth: 1
   
   references
```

## Apidoc
```eval_rst
.. toctree::
   :maxdepth: 2

   api/midnite
```

## Jupyter
```eval_rst
.. toctree::
   :maxdepth: 2
   
   notebooks/midnite
```

## Indices
```eval_rst
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
```